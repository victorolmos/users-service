# users-service

## Descripción

API REST para hacer login y listar los usuarios

## Instalación

```bash
$ npm install
```

## Configuración

La aplicación por defecto se levanta en el puerto 3000, conecta a la base de datos Mongodb en localhost, puerto 27017 y usa la clave para jwt "s3Cr3T", pero se puede cambiar en el fichero de configuración src/config.json

````
{
    "port": 3000,
    "jwtSecret": "s3Cr3T",
    "mongodb": {
        "host": "localhost",
        "port": "27017"
    }
}
````

## Arrancar la aplicación

```bash
# desarrollo
$ npm run start

# watch
$ npm run start:dev

# producción
npm run start:prod
```
## Uso

El API solamente tiene dos endpoints /login (POST) y /users (GET). Con el primero obtenemos un token para autenticarnos y con el segundo podemos recuperar un listado de usuarios.

````
$ curl -X POST "http://localhost:3000/login" -H "accept: application/json" -H "Content-Type: application/json" -d "{ \"login\": \"victor\", \"password\": \"hola\"}"

{"token":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjViMWQzZGE0MzZmMGVhNmU1MDNjOGI0NSIsImlhdCI6MTUyODY1NDEyOCwiZXhwIjoxNTI4NjU3NzI4fQ.pQrsflo7kWCXt1WWnhVbiQefZfndr2gKX-HFcpvv3Qc"}
````

````
$ curl -X GET "http://localhost:3000/users" -H "accept: application/json" -H "authorization: bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjViMWQzZGE0MzZmMGVhNmU1MDNjOGI0NSIsImlhdCI6MTUyODY1NDEyOCwiZXhwIjoxNTI4NjU3NzI4fQ.pQrsflo7kWCXt1WWnhVbiQefZfndr2gKX-HFcpvv3Qc"

[{"login":"victor","name":"Víctor Olmos Alemán","id":"5b1d3da436f0ea6e503c8b45"},{"login":"noemi","name":"Noemí Pérez García","id":"5b1d592436f0ea05ac42d3c8"}]
````

Además la aplicación levanta una web de documentación con Swagger en /api (http://localhost:3000/api/) muy util para realizar pruebas.

## Test

```bash
# unit tests
$ npm run test
```

