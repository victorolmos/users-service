import { Module } from '@nestjs/common';
import { AuthenticationModule } from './authentication/authentication.module';
import { UsersModule } from './users/users.module';
import { MongooseModule } from '@nestjs/mongoose';
const config = require('config.json');

@Module({
  imports: [AuthenticationModule, UsersModule, MongooseModule.forRoot(`mongodb://${config.mongodb.host}:${config.mongodb.port}/users`)]
})
export class AppModule {}
