import { ApiModelProperty } from "@nestjs/swagger";

export class ErrorResponse {
    @ApiModelProperty()
    statusCode: number;
    @ApiModelProperty()
    message: string;
}