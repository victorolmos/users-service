import { Get, Post, Controller, Body, UseGuards } from '@nestjs/common';
import { UsersService } from './users.service';
import { User } from './user.entity';
import { AuthGuard } from '@nestjs/passport';
import { ApiUseTags, ApiBearerAuth, ApiResponse } from '@nestjs/swagger';
import { ErrorResponse } from 'common/dto/error.response';

@ApiUseTags('users')
@ApiBearerAuth()
@Controller('/users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get()
  @UseGuards(AuthGuard('jwt'))
  @ApiResponse({ status: 200, description: 'Response Ok', type: User, isArray: true })
  @ApiResponse({ status: 401, description: 'Unauthorized', type: ErrorResponse })
  async list(): Promise<Array<User>> {
    const users = await this.usersService.findAll();
    users.forEach(user => user.password = undefined);
    return users;
  }

}
