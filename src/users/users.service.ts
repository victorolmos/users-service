import { User } from "./user.entity";
import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from 'mongoose';

@Injectable()
export class UsersService {

    constructor(@InjectModel('User') private readonly userModel: Model<User>) { }

    async findOneById(id: string): Promise<User> {
        return await this.userModel.findOne({ _id: id }).exec();
    }

    async findOneByLogin(login: string): Promise<User> {
        return await this.userModel.findOne({ login }).exec();
    }

    async findAll(): Promise<Array<User>> {
        return await this.userModel.find().exec();
    }

}