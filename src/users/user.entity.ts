import { ApiModelProperty } from "@nestjs/swagger";

export class User {
    @ApiModelProperty()
    id: string;
    @ApiModelProperty()
    login: string;
    password: string;
    @ApiModelProperty()
    name: string;
}