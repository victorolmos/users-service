import * as mongoose from 'mongoose';

export const UserSchema = new mongoose.Schema({
  _id: String,
  login: String,
  password: String,
  name: String
});

UserSchema.virtual('id').get(function(){
  return this._id;
});

UserSchema.set('toJSON', {
  virtuals: true,
  versionKey: false,
  transform: function (doc, ret) { delete ret._id }
});

UserSchema.set('toObject', {
  transform: function (doc, ret) {
    ret._id = ret.id;
    delete ret.id
  }
});
