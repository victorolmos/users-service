import { Module } from '@nestjs/common';
import { AuthenticationService } from './authentication.service';
import { JwtStrategy } from './jwt.strategy';
import { UsersModule } from '../users/users.module';
import { AuthenticationController } from './authentication.controller';

@Module({
  imports: [UsersModule],
  providers: [AuthenticationService, JwtStrategy],
  controllers: [AuthenticationController]
})
export class AuthenticationModule {}