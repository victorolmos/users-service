import { Controller, Post, UseGuards, Body } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { AuthenticationService } from './authentication.service';
import { ApiUseTags, ApiResponse } from '@nestjs/swagger';
import { LoginRequest } from './dto/login.request';
import { LoginResponse } from './dto/login.response';
import { ErrorResponse } from 'common/dto/error.response';

@ApiUseTags('users')
@Controller('login')
@ApiResponse({ status: 201, description: 'Response Ok', type: LoginResponse })
@ApiResponse({ status: 401, description: 'Unauthorized', type: ErrorResponse })
export class AuthenticationController {
    constructor(private readonly authService: AuthenticationService) { }

    @Post()
    async createToken(@Body() body: LoginRequest): Promise<LoginResponse> {
        const token = await this.authService.createToken(body.login, body.password);
        return {token};
    }

}

