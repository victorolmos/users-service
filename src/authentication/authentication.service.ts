import * as jwt from 'jsonwebtoken';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UsersService } from '../users/users.service';
import { User } from '../users/user.entity';
import * as crypto from 'crypto';
const config = require('config.json');

@Injectable()
export class AuthenticationService {

  constructor(private readonly usersService: UsersService) {}

  async createToken(login: string, password: string) {
    const user = await this.usersService.findOneByLogin(login);
    if (!user || user.password !== this.crypt(password)) {
      throw new UnauthorizedException();
    }
    return jwt.sign({id:user.id}, config.jwtSecret, { expiresIn: 3600 });
  }

  async validateUser(payload: User): Promise<User> {
    return await this.usersService.findOneById(payload.id);
  }

  private crypt(input: string): string {
    return crypto.createHash('md5').update(input).digest('hex');
  }
}