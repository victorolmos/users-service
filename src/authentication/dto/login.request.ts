import { ApiModelProperty } from "@nestjs/swagger";

export class LoginRequest {
    @ApiModelProperty()
    login: string;
    @ApiModelProperty()
    password: string;
}
